// MyPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyPaint.h"
#include <windowsx.h>
#include <winuser.h>
#include <commdlg.h>
#include <fstream>

#define MAX_LOADSTRING 100
#define MAX_LEN 260

//dll
#include "DrawLib.h"

typedef void(__cdecl *MYPROC)(HDC, int, int, int, int);

// Hàm
bool SaveFilePaint(char *filename);
bool LoadFilePaint(HWND hWnd, char *filename);


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

//dll
HINSTANCE hinstLib;
MYPROC ProcDraw;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYPAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYPAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

#include <iostream>
#include <vector>
using namespace std;
vector<int> vx1;
vector<int> vy1;
vector<int> vx2;
vector<int> vy2;
vector<int> types;
vector<DWORD> vColor;
int countShape = 0;

#define LINE 0
#define RECTANGLE 1
#define CIRCLE 2

class CShape {
private:
	HPEN priColor;
public:
	virtual void Draw(HDC hdc) = 0;
	virtual CShape* Create() = 0;
	virtual void SetData(int a, int b, int c, int d) = 0;
	void SetHpen(HPEN color) {
		priColor = color;
	}
	HPEN GetHpen()
	{
		return priColor;
	}
};

class CLine : public CShape {
private:
	int x1;
	int y1;
	int x2;
	int y2;
public:
	void Draw(HDC hdc) {
		DrawLibrary::Draw::DrawLine(hdc, x1, y1, x2, y2);
	}

	CShape* Create() { return new CLine; }

	void SetData(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}

};

class CRectangle : public CShape {
private:
	int x1;
	int y1;
	int x2;
	int y2;
public:
	void Draw(HDC hdc) {
		DrawLibrary::Draw::DrawRectangle(hdc, x1, y1, x2, y2);
	}

	CShape* Create() { return new CRectangle; }

	void SetData(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
};

class CCircle : public CShape {
private:
	int x1;
	int y1;
	int x2;
	int y2;
public:
	void Draw(HDC hdc)
	{
		DrawLibrary::Draw::DrawCircle(hdc, x1, y1, x2, y2);
	}

	CShape* Create()
	{
		return new CCircle;
	}

	void SetData(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
};

vector<CShape*> shapes;
vector<CShape*> prototypes;
HMENU hMenu;
HBRUSH hbr;
RECT rc;
HDC hdc;
HPEN hPen;
bool isDrawing = FALSE;
int isShape;
int currentX;
int currentY;
int lastX;
int lastY;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;
	hMenu = GetMenu(hWnd);

	//////////////////////////////
	CHOOSECOLOR color;
	static COLORREF colors[16];
	static DWORD rgbCurrent;
	////////////////////////////

	//////////////////////////////
	OPENFILENAME openFile;
	char filename[MAX_LEN];
	wchar_t tempFile[260];
	size_t i;

    switch (message)
    {
	case WM_CREATE:
	{
		prototypes.push_back(new CLine);
		prototypes.push_back(new CRectangle);
		prototypes.push_back(new CCircle);
		hinstLib = LoadLibrary(L"MyDLL.dll");
		break;
	}
	break;
	case WM_LBUTTONDOWN:
	{
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		if (!isDrawing) {
			isDrawing = TRUE;
			currentX = x;
			currentY = y;
		}

		//Nhấp chuột lưu đã vẽ 1 hình
		//Tăng biến countShape lên
		countShape++;
		vx1.push_back(x); //Lưu tọa độ x đầu
		vy1.push_back(y); //Lưu tọa độ y đầu

	}
	break;
	case WM_MOUSEMOVE:
	{
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);

		if (isDrawing) {
			lastX = x;
			lastY = y;
		}
		InvalidateRect(hWnd, NULL, TRUE);
		UpdateWindow(hWnd);
	} 
	break;
	case WM_LBUTTONUP: 
	{
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);

		// Lưu tọa độ
		vx2.push_back(x); //Lưu tọa độ x cuối
		vy2.push_back(y); //Lưu tọa độ y cuối

		// Lưu type
		types.push_back(isShape);

		// Lưu màu
		hPen = CreatePen(PS_GEOMETRIC, 3, rgbCurrent);
		vColor.push_back(rgbCurrent);
		if (0 == isShape)
		{
			CLine* line = new CLine;
			line->SetData(currentX, currentY, x, y);
			line->SetHpen(hPen);
			shapes.push_back(line);
		}
		else if(1 == isShape) {
			CRectangle *rec = new CRectangle;
			rec->SetData(currentX, currentY, x, y);
			rec->SetHpen(hPen);
			shapes.push_back(rec);
		}
		else if (2 == isShape) {
			CCircle *cir = new CCircle;
			cir->SetData(currentX, currentY, x, y);
			cir->SetHpen(hPen);
			shapes.push_back(cir);
		}

		isDrawing = FALSE;

		InvalidateRect(hWnd, NULL, TRUE);

	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);

		CShape* shape;

		// Parse the menu selections:
		switch (wmId)
		{
		case ID_DRAW_LINE:
			shape = prototypes[LINE]->Create();
			shapes.push_back(shape);
			isShape = LINE;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_DRAW_RECTANGLE:
			shape = prototypes[RECTANGLE]->Create();
			shapes.push_back(shape);
			isShape = RECTANGLE;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_DRAW_CIRCLE:
			shape = prototypes[CIRCLE]->Create();
			shapes.push_back(shape);
			isShape = CIRCLE;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_COLOR:
			ZeroMemory(&color, sizeof(color));
			color.lStructSize = sizeof(color);
			color.hwndOwner = hWnd;
			color.lpCustColors = (LPDWORD)colors;
			color.rgbResult = rgbCurrent;
			color.Flags = CC_FULLOPEN | CC_RGBINIT;

			if (ChooseColor(&color) == TRUE)
			{
				hbr = CreateSolidBrush(color.rgbResult);
				rgbCurrent = color.rgbResult;

				InvalidateRect(hWnd, NULL, TRUE);
				UpdateWindow(hWnd);
			}
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_FILE_SAVE:
		{
			ZeroMemory(&openFile, sizeof(openFile));
			openFile.lStructSize = sizeof(openFile);
			openFile.hwndOwner = hWnd;
			openFile.nMaxFile = MAX_LEN;
			openFile.lpstrFile = tempFile;
			openFile.lpstrFile[0] = '\0';
			openFile.lpstrFilter = L"Text Files(*.txt)\0 * .txt\0All Files(*.*)\0 * .*\0";
			openFile.lpstrInitialDir = NULL;
			openFile.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

			if (TRUE == GetSaveFileName(&openFile))
			{
				wcstombs_s(&i, filename, openFile.lpstrFile, MAX_LEN);
			}

			if (FALSE == SaveFilePaint(filename))
			{
				MessageBox(hWnd, L"Không thể lưu file", L"Error", MB_OK);
			}
		}
		break;
		case ID_FILE_LOAD:
		{
			// Vì có thể đang vẽ nên biến countShape đã tăng
			// Nên reset lại giá trị
			countShape = 0;

			// Các giá trị tọa độ có thể đã đc thêm nên ta cần xóa hết
			vx1.clear();
			vx2.clear();
			vy1.clear();
			vy2.clear();
			vColor.clear();
			shapes.clear();
			types.clear();

			// Mở menu chọn file
			ZeroMemory(&openFile, sizeof(openFile));
			openFile.lStructSize = sizeof(openFile);
			openFile.hwndOwner = hWnd;
			openFile.nMaxFile = MAX_LEN;
			openFile.lpstrFile = tempFile;
			openFile.lpstrFile[0] = '\0';
			openFile.lpstrFilter = L"Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
			openFile.nFilterIndex = 2;
			openFile.lpstrInitialDir = NULL;
			openFile.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

			// Lấy tên file
			if (GetOpenFileName(&openFile) == TRUE)
			{
				wcstombs_s(&i, filename, openFile.lpstrFile, MAX_LEN);
			}

			// Tiến hành load paint
			if (LoadFilePaint(hWnd, filename) == FALSE)
				MessageBox(hWnd, L"Không thể load file", L"Error", MB_OK);
		}
		break;
		case ID_FILE_NEW:
		{
			// Có thời đang vẽ mà người dùng nhấn New
			// Nên ta reset lại các thông số về giá trị ban đầu
			countShape = 0;
			vx1.clear();
			vy1.clear();
			vx2.clear();
			vy2.clear();
			types.clear();
			vColor.clear();
			shapes.clear();

			//Xóa màn hình
			FillRect(hdc, &rc, (HBRUSH)GetStockBrush(WHITE_BRUSH));
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);


		for (int i = 0; i < shapes.size(); i++) {
			SelectObject(hdc, shapes[i]->GetHpen());
			shapes[i]->Draw(hdc);
		}

		// Lấy màu
		hPen = CreatePen(PS_GEOMETRIC, 3, rgbCurrent);
		SelectObject(hdc, hPen);

		if (isDrawing) {
			if (0 == isShape)
			{
				//MoveToEx(hdc, currentX, currentY, NULL);
				//LineTo(hdc, lastX, lastY);
				ProcDraw = (MYPROC)GetProcAddress(hinstLib, "DrawLine");
				if (ProcDraw != NULL)
				{
					ProcDraw(hdc, currentX, currentY, lastX, lastY);
				}
			}
			else if (1 == isShape)
			{
				Rectangle(hdc, currentX, currentY, lastX, lastY);
				ProcDraw = (MYPROC)GetProcAddress(hinstLib, "DrawRectangle");
				if (ProcDraw != NULL)
				{
					ProcDraw(hdc, currentX, currentY, lastX, lastY);
				}
			}
			else if (2 == isShape)
			{
				Ellipse(hdc, currentX, currentY, lastX, lastY);
				ProcDraw = (MYPROC)GetProcAddress(hinstLib, "DrawCircle");
				if (ProcDraw != NULL)
				{
					ProcDraw(hdc, currentX, currentY, lastX, lastY);
				}
			}
		}

		EndPaint(hWnd, &ps);
	}
	break;
	
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

bool SaveFilePaint(char *filename)
{
	ofstream ofile;
	ofile.open(filename);
	if (!ofile)
	{
		return FALSE;
	}

	//Ghi từng thông số vào file cách nhau bởi dâu cách
	for (int i = 0; i < countShape; i++)
	{
		//		hình gì?			màu gì?
		ofile << types[i] << " " <<  vColor[i] << " "
			<< vx1[i] << " " << vy1[i] << " " << vx2[i] << " " << vy2[i] << endl;
	}

	ofile.close();
	return TRUE;
}

bool LoadFilePaint(HWND hWnd, char *filename)
{
	PAINTSTRUCT ps;
	HDC loadhdc = BeginPaint(hWnd, &ps);
	ifstream ifile;
	ifile.open(filename);
	if (!ifile)
	{
		return FALSE;
	}
	int x1, y1, x2, y2; // Tọa độ
	int count = 0; // Đếm số hình đã vẽ
	int type; // Loại hình
	DWORD color; // Màu của hình

	// Tiến hành đọc file
	while (!ifile.eof())
	{
		// Lấy dần các thông số
		ifile >> type;
		ifile >> color;
		ifile >> x1;
		ifile >> y1;
		ifile >> x2;
		ifile >> y2;

		// Tạo màu tương ứng vs color
		hPen = CreatePen(PS_GEOMETRIC, 3, color);

		// Tiến hành phân loại và vẽ
		if (0 == type)
		{
			CLine* line = new CLine;
			line->SetData(x1, y1, x2, y2);
			line->SetHpen(hPen);
			shapes.push_back(line);
		}
		else if (1 == type)
		{
			CRectangle *rec = new CRectangle;
			rec->SetData(x1, y1, x2, y2);
			rec->SetHpen(hPen);
			shapes.push_back(rec);
		}
		else if (2 == type)
		{
			CCircle *cir = new CCircle;
			cir->SetData(x1, y1, x2, y2);
			cir->SetHpen(hPen);
			shapes.push_back(cir);
		}
		else {
			//Do nothing
		}
		// Vẽ lại những hình đã load
		for (int i = 0; i < countShape; i++)
		{
			SelectObject(loadhdc, shapes[i]->GetHpen());
			shapes[i]->Draw(loadhdc);
		}


		InvalidateRect(hWnd, NULL, TRUE);
		UpdateWindow(hWnd);
		count++;
	}
	ifile.close();
	return TRUE;
}