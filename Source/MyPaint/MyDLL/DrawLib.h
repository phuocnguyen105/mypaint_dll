#pragma once

#ifdef DRAWLIBRARY_EXPORTS
#define DRAWLIBRARY_API __declspec(dllexport)

#else
#define DRAWLIBRARY_API __declspec(dllimport)
#endif // DRAWLIBRARY_EXPORTS

namespace DrawLibrary
{
	class Draw
	{
	public:
		static DRAWLIBRARY_API void DrawLine(HDC hdc, int x1, int y1, int x2, int y2);
		static DRAWLIBRARY_API void DrawRectangle(HDC hdc, int x1, int y1, int x2, int y2);
		static DRAWLIBRARY_API void DrawCircle(HDC hdc, int x1, int y1, int x2, int y2);

	};
}

extern "C" DRAWLIBRARY_API void DrawNewLine(HDC hdc, int x1, int y1, int x2, int y2);
extern "C" DRAWLIBRARY_API void DrawNewRectangle(HDC hdc, int x1, int y1, int x2, int y2);
extern "C" DRAWLIBRARY_API void DrawNewCircle(HDC hdc, int x1, int y1, int x2, int y2);
