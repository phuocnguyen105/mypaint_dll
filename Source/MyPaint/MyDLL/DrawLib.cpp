#include "stdafx.h"
#include "DrawLib.h"

namespace DrawLibrary
{
	void Draw::DrawLine(HDC hdc, int a, int b, int c, int d)
	{
		MoveToEx(hdc, a, b, NULL);
		LineTo(hdc, c, d);
	}

	void Draw::DrawRectangle(HDC hdc, int a, int b, int c, int d)
	{
		Rectangle(hdc, a, b, c, d);
	}

	void Draw::DrawCircle(HDC hdc, int a, int b, int c, int d)
	{
		Ellipse(hdc, a, b, c, d);
	}
}

void DrawNewLine(HDC hdc, int a, int b, int c, int d)
{
	MoveToEx(hdc, a, b, NULL);
	LineTo(hdc, c, d);
}

void DrawNewRectangle(HDC hdc, int a, int b, int c, int d)
{
	Rectangle(hdc, a, b, c, d);
}

void DrawNewCircle(HDC hdc, int a, int b, int c, int d)
{
	Ellipse(hdc, a, b, c, d);
}